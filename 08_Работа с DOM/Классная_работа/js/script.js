const url = "https://old.bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"

const parseData = (data) => {
    data.forEach(element => {
        const list = document.getElementById("list");
        const item = document.createElement("li");
        item.innerHTML = `Курс ${element.txt} (${element.cc}) на дату: ${element.exchangedate} = ${element.rate}`;
        list.appendChild(item);
        const itemStyle = `color:${getRandomColor()}`;
        item.setAttribute("style", itemStyle);
    });
}

const getRandomColor = () => {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

$.getJSON(url, parseData);