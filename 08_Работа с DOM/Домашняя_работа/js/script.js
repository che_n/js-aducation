
const bttn = document.querySelector(".bttn")
bttn.onclick = () => {
  const divs = document.querySelectorAll("div")
  if(divs.length >= 10){
    let tempArr = [...divs]
    while(tempArr.length){
      const element = tempArr.shift();
      document.body.removeChild(element)
    }
  }else{
    var newElement = document.createElement("div");
    newElement.innerHTML = `This element has number ${divs.length + 1}`;
    document.body.appendChild(newElement);
  }
}